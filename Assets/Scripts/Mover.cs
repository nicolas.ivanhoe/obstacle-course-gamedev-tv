using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] float moveSpeed = 2.5f;
    [SerializeField] float maxSpeed = 6f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MovePlayer();
    }

    void Update() 
    {
        if (Time.time > 1f)    
        {
            Debug.Log("Freezing Position Y");
            rb.constraints = RigidbodyConstraints.FreezePositionY;
        }
    }

    void MovePlayer()
    {
        float xValue = Input.GetAxis("Horizontal") * moveSpeed;
        float zValue = Input.GetAxis("Vertical") * moveSpeed;

        rb.AddForce(xValue, 0, 0);
        rb.AddForce(0, 0, zValue);

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
    }
}
